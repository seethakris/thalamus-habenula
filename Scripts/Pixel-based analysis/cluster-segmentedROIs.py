"""
Standalone script for clustering ROIs segmented using imageJ.
Preprocessing, clustering, labeling clusters by repsonse is done in these functions.
Use the __name__ == '__main__' call below to enter user inputs for running the function

"""
import os
import scipy.io
import numpy as np
import matplotlib.pyplot as plt
import time
from scipy.cluster.vq import kmeans, vq
from scipy.signal import detrend
from scipy.stats import sem
import seaborn as sns
import pandas as pd
import scikits.bootstrap as bootstrap
from copy import copy

sns.set_context('paper', font_scale=2)



class GetData(object):
    def __init__(self, WorkingDirectory, baseline, stimulus_on_time, stimulus_off_time, smooth_window,
                 zscore_threshold):

        self.WorkingDirectory = WorkingDirectory
        self.stimulus_on_time = stimulus_on_time
        self.stimulus_off_time = stimulus_off_time
        self.smooth_window = smooth_window
        self.numclusters = numclusters
        self.zscore_threshold = zscore_threshold
        self.baseline = baseline

    def LoadData(self):
        mat_filenames = [f for f in os.listdir(self.WorkingDirectory) if f.endswith('.mat')]

        for ii in mat_filenames:
            print 'Collecting File...', ii
            # Load Data
            alldata = scipy.io.loadmat(os.path.join(self.WorkingDirectory, ii))
            calcium = alldata['CellZ5'][11:, :]
            row = alldata['CellZ5'][3, :]
            # Preprocess data
            # row, calcium = self.remove_neuropils(alldata)
            calcium_detrend = self.detrend_calcium_trace(calcium)  # Detrend
            calcium_dfof = self.normalize(calcium_detrend)  # Normalize

            # Prepare Data for saving - First three rows correspond to cell number, row number, type of response
            calcium_data_for_saving = self.prepare_data_for_saving(calcium_dfof, row)
            calcium_smooth = self.smooth_calcium_trace(calcium_dfof, windowlen=self.smooth_window)  # Smooth data
            self.plot_calcium_signals(calcium_smooth, filename=ii)  # Plot data

            calcium_data_for_saving[3:, :] = calcium_smooth
            np.savetxt(os.path.join(self.WorkingDirectory, 'ClusteringSignals', ii[:-4] + 'calcium_smooth.csv'),
                       calcium_data_for_saving, delimiter=",")

    def prepare_data_for_saving(self, data, row):

        data_for_saving = np.zeros((np.size(data, 0) + 3, np.size(data, 1)))
        data_for_saving[0, :] = np.asarray(range(0, np.size(data, 1)))
        data_for_saving[1, :] = (row < 4).astype(int)

        # Threshold and remove noisy signals
        data_for_saving[2, :] = 1 - (np.max(data, 0) > self.zscore_threshold).astype(int)
        calcium_signals_thresh = data[:, np.max(data, 0) > self.zscore_threshold]
        print "Before Thresh %s After Thresh %s " % (np.shape(data), np.shape(calcium_signals_thresh))
        return data_for_saving

    def plot_calcium_signals(self, calcium_trace, filename):

        # Plot some stuff
        fs = plt.figure()
        gs = plt.GridSpec(2, 1, height_ratios=[2, 1])

        fs.add_subplot(gs[0, :])
        plt.imshow(calcium_trace.T, cmap='seismic', aspect='auto', vmin=-2, vmax=2)
        plt.colorbar()
        PlottingTools.plot_vertical_lines_onset(self.stimulus_on_time)
        PlottingTools.plot_vertical_lines_offset(self.stimulus_off_time)

        fs.add_subplot(gs[1, :])
        plt.plot(np.mean(calcium_trace, 1))
        plt.xlim((0, np.size(calcium_trace, 0)))
        PlottingTools.plot_vertical_lines_onset(self.stimulus_on_time)
        PlottingTools.plot_vertical_lines_offset(self.stimulus_off_time)
        plt.tight_layout()

        plt.savefig('/Users/seetha/Desktop/Temp_Figures/' + filename[:-4] + 'smooth.png')

    def normalize(self, calicum_trace):
        mean = np.mean(calicum_trace[self.baseline[0]:self.baseline[1], :], 0)
        standarddeviation = np.std(calicum_trace[self.baseline[0]:self.baseline[1], :], 0)
        normalizedcalcium = (calicum_trace - mean) / (standarddeviation + 0.001)

        return normalizedcalcium

    def remove_neuropils(self, data):
        # Seperate neuropil data
        calcium = data['CellZ5'][11:, :]
        row = data['CellZ5'][3, :]
        neuropil = data['NeuropilList']

        calcium_from_cells = np.array([])
        row_cells = np.array([])
        for jj in xrange(0, 5):
            C1 = calcium[:, row == jj + 1]
            C2 = C1[:, np.squeeze(neuropil[0][jj] == 0)]

            R1 = row[row == jj + 1]
            R2 = R1[np.squeeze(neuropil[0][jj] == 0)]

            calcium_from_cells = np.hstack((calcium_from_cells, C2)) if calcium_from_cells.size else C2
            row_cells = np.hstack((row_cells, R2)) if row_cells.size else R2
        return row_cells, calcium_from_cells

    def detrend_calcium_trace(self, calcium_trace):
        detrended = np.zeros(np.shape(calcium_trace))
        for ss in xrange(0, np.size(calcium_trace, 1)):
            detrended[:, ss] = detrend(calcium_trace[:, ss])
        return detrended

    def smooth_calcium_trace(self, calcium_trace, windowlen):
        smoothed_calcium_trace = np.zeros(np.shape(calcium_trace))
        for ss in xrange(0, np.size(calcium_trace, 1)):
            smoothed_calcium_trace[:, ss] = PlottingTools.smooth_hanning(calcium_trace[:, ss], windowlen, window='flat')
        return smoothed_calcium_trace


class PlottingTools(object):
    @classmethod
    def plot_vertical_lines_onset(cls, stimulus_on_time, **kwargs):
        if 'axis' in kwargs:
            ax = kwargs['axis']
            for ii in xrange(0, np.size(stimulus_on_time)):
                ax.axvline(x=stimulus_on_time[ii], linestyle='-', color='k', linewidth=2)
        else:
            for ii in xrange(0, np.size(stimulus_on_time)):
                plt.axvline(x=stimulus_on_time[ii], linestyle='-', color='k', linewidth=2)

    @classmethod
    def plot_vertical_lines_offset(cls, stimulus_off_time, **kwargs):
        if 'axis' in kwargs:
            ax = kwargs['axis']
            for ii in xrange(0, np.size(stimulus_off_time)):
                ax.axvline(x=stimulus_off_time[ii], linestyle='--', color='k', linewidth=2)
        else:
            for ii in xrange(0, np.size(stimulus_off_time)):
                plt.axvline(x=stimulus_off_time[ii], linestyle='--', color='k', linewidth=2)

    @classmethod
    def smooth_hanning(cls, x, window_len, window='hanning'):
        s = np.r_[x[window_len - 1:0:-1], x, x[-1:-window_len:-1]]
        if window == 'flat':  # moving average
            w = np.ones(window_len, 'd')
        else:
            w = np.hanning(window_len)
        y = np.convolve(w / w.sum(), s, mode='valid')
        return y[:-window_len + 1]


class ClusterwithKmeans(object):
    def __init__(self, WorkingDirectory, baseline, numclusters, stimulus_on_time, stimulus_off_time, responsetypeindex,
                 responsetype_label):
        self.numclusters = numclusters
        self.WorkingDirectory = WorkingDirectory
        self.stimulus_on_time = stimulus_on_time
        self.stimulus_off_time = stimulus_off_time
        self.responsetypeindex = responsetypeindex
        self.responsetype_label = responsetype_label
        self.baseline = baseline

    def Kmeans(self):
        csv_filenames = [f for f in os.listdir(os.path.join(self.WorkingDirectory, 'ClusteringSignals')) if
                         f.endswith('.csv')]

        count_files = 0
        cellnumber = np.zeros((np.size(self.responsetypeindex), np.size(csv_filenames)))
        fluoresencebaseline = np.zeros((np.size(self.responsetypeindex), np.size(csv_filenames)))
        fluoresenceON = np.zeros((np.size(self.responsetypeindex), np.size(csv_filenames)))
        fluoresenceOFF = np.zeros((np.size(self.responsetypeindex), np.size(csv_filenames)))

        for ii in csv_filenames:
            print 'Clustering Data', ii
            alldata = np.loadtxt(os.path.join(self.WorkingDirectory, 'ClusteringSignals', ii), delimiter=",")
            calcium = alldata[3:, :]

            experiment_time = np.size(calcium, 0)
            number_of_cells = np.size(calcium, 1)

            clustercentroids, clusterlabels = self.run_kmeans(calcium.T)  # Run Kmeans
            self.plot_meankmeanclusters(calcium.T, clusterlabels, filename=ii)  # Plot clusters

            stimulus_trace_for_correlation = self.traces_for_correlation(experiment_time,
                                                                         filename=ii)  # Get stimulus trace to correlate
            correlation_coefficient = self.correlate_with_clusters(calcium.T, clusterlabels,
                                                                   stimulus_trace_for_correlation, filename=ii)

            updated_alldata = self.classify_cells(alldata, clusterlabels, correlation_coefficient)
            self.plot_clustertraces(updated_alldata.T, clusterlabels, filename=ii)

            cellnumber[:, count_files], fluoresencebaseline[:, count_files], \
            fluoresenceON[:, count_files], fluoresenceOFF[:, count_files] \
                = self.get_mean_ofcells_and_activity(updated_alldata.T)

            count_files += 1

        self.savedataascsv(cellnumber, fluoresencebaseline, fluoresenceON, fluoresenceOFF)

    def Kmeans_ALL(self):
        csv_filenames = [f for f in os.listdir(os.path.join(self.WorkingDirectory, 'ClusteringSignals')) if
                         f.endswith('.csv')]

        count_files = 0

        for ii in csv_filenames:
            alldata = np.loadtxt(os.path.join(self.WorkingDirectory, 'ClusteringSignals', ii), delimiter=",")
            calcium = alldata[3:, :]

            if count_files == 0:
                calcium_all = copy(calcium)
                alldata_all = copy(alldata)
            else:
                calcium_all = np.hstack([calcium_all, calcium])
                alldata_all = np.hstack([alldata_all, alldata])

            count_files += 1

        experiment_time = np.size(calcium_all, 0)

        clustercentroids, clusterlabels = self.run_kmeans(calcium_all.T)  # Run Kmeans
        self.plot_meankmeanclusters(calcium_all.T, clusterlabels, filename='All.mat')  # Plot clusters

        stimulus_trace_for_correlation = self.traces_for_correlation(experiment_time,
                                                                     filename='All.mat')  # Get stimulus trace to correlate
        correlation_coefficient = self.correlate_with_clusters(calcium_all.T, clusterlabels,
                                                               stimulus_trace_for_correlation, filename='All.mat')

        updated_alldata = self.classify_cells(alldata_all, clusterlabels, correlation_coefficient)
        self.plot_clustertraces(updated_alldata.T, clusterlabels, filename='All.mat')

        print 'Updated_all_Data...%s, Updated Cluster Labels %s' % (
            np.shape(updated_alldata), np.shape(clusterlabels))

    def run_kmeans(self, data):
        centroids, _ = kmeans(data, self.numclusters)  # computing K-Means
        idx, _ = vq(data, centroids)  # assign each sample to a cluster
        return centroids, idx

    def traces_for_correlation(self, time, filename):
        # Define stimulus traces for ON and OFF (for now)
        stim_type = ['ON', 'OFF', 'INHIBITION']
        stim_traces = np.zeros((time, np.size(stim_type)))
        stim_time = self.stimulus_off_time[0] - self.stimulus_on_time[0]

        print 'Stimulus interval, ', stim_time

        for ii in xrange(0, np.size(stim_type)):
            for jj in xrange(0, np.size(self.stimulus_on_time)):
                if stim_type[ii] == 'ON':
                    stim_traces[self.stimulus_on_time[jj]:self.stimulus_off_time[jj], ii] = 10
                elif stim_type[ii] == 'OFF':
                    if jj == np.size(self.stimulus_on_time) - 1:
                        stim_traces[self.stimulus_off_time[jj]:self.stimulus_off_time[jj] + stim_time, ii] = 10
                    else:
                        stim_traces[self.stimulus_off_time[jj]:self.stimulus_on_time[jj + 1], ii] = 10
                elif stim_type[ii] == 'INHIBITION':
                    stim_traces[self.stimulus_on_time[jj]:self.stimulus_off_time[jj], ii] = -3

        stim_traces_smooth = np.zeros((np.shape(stim_traces)))
        for ii in xrange(0, np.size(stim_type)):
            stim_traces_smooth[:, ii] = PlottingTools.smooth_hanning(stim_traces[:, ii], window_len=10,
                                                                     window='hanning')
        plt.figure()
        plt.plot(stim_traces_smooth)
        plt.ylim((-5, 14))
        plt.savefig('/Users/seetha/Desktop/Temp_Figures/' + filename[:-4] + 'smoothcorrelationtrace.png',
                    bbox_inches='tight')
        plt.close()
        return stim_traces_smooth

    def correlate_with_clusters(self, data, idx, stimulustrace, filename):
        correlation_coeff = np.zeros((self.numclusters, np.size(stimulustrace, 1)))
        for ii in xrange(0, self.numclusters):
            datatrace = np.mean(data[idx == ii, :], 0)
            for jj in xrange(0, np.size(stimulustrace, 1)):
                stimulus = stimulustrace[:, jj]
                correlation_coeff[ii, jj] = np.corrcoef(datatrace, stimulus)[0, 1]

        plt.figure()
        plt.imshow(correlation_coeff, cmap='seismic', aspect='auto', interpolation='None', vmin=-1, vmax=1)
        plt.grid('off')
        plt.colorbar()
        plt.savefig('/Users/seetha/Desktop/Temp_Figures/' + filename[:-4] + 'correlationcoeff.png', bbox_inches='tight')
        plt.close()
        return correlation_coeff

    def plot_clustertraces(self, data, idx, filename):
        num_such_clusters = np.zeros((self.numclusters, 1))
        label_clusters = np.zeros((self.numclusters, 1))

        colors = sns.color_palette("Paired", self.numclusters)
        fig1 = plt.figure(figsize=(20, 20))
        gs = plt.GridSpec(4, 1)
        gs.update(hspace=1)

        for jj in xrange(0, self.numclusters):
            num_such_clusters[jj] = np.shape(data[idx == jj, :])[0]
            label_clusters[jj] = int(data[idx == jj, 2][1])

            x = np.linspace(0, np.size(data[:, 3:], 1), np.size(data[:, 3:], 1))
            y = np.mean(data[idx == jj, 3:], 0)
            error = sem(data[idx == jj, 3:], 0)

            print 'x %s, y %s, error %s ' % (np.shape(x), np.shape(y), np.shape(error))
            print 'label %s' % (int(data[idx == jj, 2][1]))
            if label_clusters[jj] != 1:
                ax1 = fig1.add_subplot(gs[int(data[idx == jj, 2][1]) - 1, 0])
                ax1.plot(x, y,
                         label='Cluster %s Cells %s Type %s' % (jj, num_such_clusters[jj], label_clusters[jj]),
                         color=colors[jj], linewidth=4)

                ax1.fill_between(x, y - error, y + error, color=colors[jj], alpha=0.5)
                ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))

                ax1.set_xlim((0, np.size(data, 1)))
                ax1.set_ylim((-3, 15))
                plt.axhline(y=0, linestyle='-', color='k', linewidth=1)
                ax1.locator_params(axis='y', nbins=6)
                ax1.set_xlabel('Time (seconds)')
                ax1.set_ylabel('Z-Score')

                PlottingTools.plot_vertical_lines_onset(self.stimulus_on_time)
                PlottingTools.plot_vertical_lines_offset(self.stimulus_off_time)

        # plt.tight_layout()

        fig1.savefig('/Users/seetha/Desktop/Temp_Figures/' + filename[:-4] + 'kmean_labels.png', bbox_inches='tight')
        fig1.savefig('/Users/seetha/Desktop/Temp_Figures/' + filename[:-4] + 'kmean_labels.pdf', bbox_inches='tight',
                     dpi=300)

        plt.close()

    def plot_meankmeanclusters(self, data, idx, filename):
        # some plotting using numpy's logical indexing
        plt.figure()
        num_such_clusters = np.zeros((self.numclusters, 1))
        colors = sns.color_palette("Paired", self.numclusters)
        for jj in xrange(0, self.numclusters):
            num_such_clusters[jj] = np.shape(data[idx == jj, :])[0]
            plt.plot(np.mean(data[idx == jj, :], 0),
                     label='Cluster %s Cells %s' % (jj, num_such_clusters[jj]), color=colors[jj], linewidth=4)
            plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        plt.xlim((0, np.size(data, 1)))
        PlottingTools.plot_vertical_lines_onset(self.stimulus_on_time)
        PlottingTools.plot_vertical_lines_offset(self.stimulus_off_time)
        plt.savefig('/Users/seetha/Desktop/Temp_Figures/' + filename[:-4] + 'kmean.png', bbox_inches='tight')
        plt.close()

    def classify_cells(self, data, idx, correlation_coefficient):  # Data = complete dataset
        max_correlation = np.argmax(correlation_coefficient[:, 0:2], 1)  # Only take On and OFF responses
        # Sort inhibition responses now
        max_correlation_w_inhibition = np.argmax(correlation_coefficient, 1)
        low_correlation_values = (correlation_coefficient[:, 0:2] < 0.3).all(axis=1).astype(int)

        for ii in xrange(0, np.size(correlation_coefficient, 0)):
            if low_correlation_values[ii] == 1:
                print 'No response is ', ii
                data[2, idx == ii] = self.responsetypeindex[0]  # Classify as no response
            elif max_correlation_w_inhibition[ii] == 2 and correlation_coefficient[ii, 0] < 0:
                print 'Inhibitory Cluster is ', ii
                data[2, idx == ii] = self.responsetypeindex[3]  # Inhibitory responses
            elif max_correlation[ii] == 0:
                data[2, idx == ii] = self.responsetypeindex[1]  # ON responses
            elif max_correlation[ii] == 1:
                data[2, idx == ii] = self.responsetypeindex[2]  # OFF responses
        return data

    def get_mean_ofcells_and_activity(self, data):
        num_such_clusters = np.zeros((np.size(responsetype_idx)))
        baseline_value = np.zeros((np.size(responsetype_idx)))
        fluoresence_value_ON = np.zeros((np.size(responsetype_idx)))
        fluoresence_value_OFF = np.zeros((np.size(responsetype_idx)))

        for ii in xrange(0, np.size(self.responsetypeindex)):
            response_data = data[data[:, 2] == self.responsetypeindex[ii], 3:]
            num_such_clusters[ii] = (float(np.size(response_data, 0)) / float(np.size(data, 0))) * 100  # of cells

            if num_such_clusters[ii] == 0:
                continue

            baseline_value[ii] = np.mean(response_data[:, self.baseline[0]:self.baseline[1]])

            temp_ON = []
            for jj in xrange(0, np.size(self.stimulus_on_time)):
                temp_ON.append(response_data[:, self.stimulus_on_time[jj]:self.stimulus_off_time[jj]])

            temp_OFF = []
            for jj in xrange(0, np.size(self.stimulus_off_time)):
                if jj == np.size(self.stimulus_off_time) - 1:
                    temp_OFF.append(response_data[:, self.stimulus_off_time[jj]:20])
                else:
                    temp_OFF.append(response_data[:, self.stimulus_off_time[jj]:self.stimulus_on_time[jj + 1]])

            if self.responsetype_label[ii] == 'ON' or self.responsetype_label[ii] == 'Inhibitory':
                fluoresence_value_ON[ii] = np.median(np.asarray(np.hstack(temp_ON)))
                fluoresence_value_OFF[ii] = np.median(np.asarray(np.hstack(temp_OFF)))
            elif self.responsetype_label[ii] == 'OFF':
                fluoresence_value_ON[ii] = np.median(np.asarray(np.hstack(temp_ON)))
                fluoresence_value_OFF[ii] = np.median(np.asarray(np.hstack(temp_OFF)))

            else:
                fluoresence_value_ON[ii] = np.median(np.asarray(np.hstack(temp_ON)))
                fluoresence_value_OFF[ii] = np.median(np.asarray(np.hstack(temp_OFF)))

        return num_such_clusters, baseline_value, fluoresence_value_ON, fluoresence_value_OFF

    def savedataascsv(self, cellnumber, fluoresencebaseline, fluoresenceON, fluoresenceOFF):
        # Get mean of everything
        # compute 95% confidence intervals around the mean
        cellnumber = [np.round(np.median(cellnumber, 1), 2), np.round(np.std(cellnumber, 1), 2)]
        fluoresencebaseline = [np.round(np.median(fluoresencebaseline, 1), 2),
                               np.round(np.std(fluoresencebaseline, 1), 2)]
        fluoresenceON = [np.round(np.median(fluoresenceON, 1), 2), np.round(np.std(fluoresenceON, 1), 2)]
        fluoresenceOFF = [np.round(np.median(fluoresenceOFF, 1), 2), np.round(np.std(fluoresenceOFF, 1), 2)]

        Dataframe = pd.DataFrame({'Response Type ': self.responsetype_label,
                                  '% of Cells Mean': cellnumber[0], '% of Cells Stddev': cellnumber[1],
                                  'Baseline Fluoresence Mean': fluoresencebaseline[0],
                                  'Baseline Fluoresence Stddev': fluoresencebaseline[1],
                                  'ON Fluoresence Mean': fluoresenceON[0],
                                  'ON Fluoresence Stdev': fluoresenceON[1],
                                  'OFF Fluoresence Mean': fluoresenceOFF[0],
                                  'OFF Fluoresence Stdev': fluoresenceOFF[1]})

        Dataframe.to_csv(os.path.join(self.WorkingDirectory, 'Cellstats', 'cell_stats.csv'), delimiter=',')


if __name__ == '__main__':

    # User input
    WorkingDirectory = '/Users/seetha/Desktop/Modelling/Data/Network/'
    stimulus_on_time = [46, 98, 142, 194]
    stimulus_off_time = [65, 117, 161, 213]
    stimulus_on_time = [jj - 2 for jj in stimulus_on_time]
    stimulus_off_time = [jj - 2 for jj in stimulus_off_time]
    baseline = [10, 30]
    numclusters = 50
    smooth_window = 4
    zscore_threshold = 3
    responsetype_idx = [1, 2, 3, 4]  # No Response, ON, OFF, Inhibitory
    responsetype_label = ['No Response', 'ON', 'OFF', 'Inhibitory']

    if not os.path.exists(os.path.join(WorkingDirectory, 'ClusteringSignals')):
        os.makedirs(os.path.join(WorkingDirectory, 'ClusteringSignals'))
    if not os.path.exists(os.path.join(WorkingDirectory, 'Cellstats')):
        os.makedirs(os.path.join(WorkingDirectory, 'Cellstats'))

    # Get save and plot data
    GetData(WorkingDirectory, baseline, stimulus_on_time, stimulus_off_time, smooth_window, zscore_threshold).LoadData()

    # Run Kmeans
    HabenulaCluster = ClusterwithKmeans(WorkingDirectory, baseline, numclusters, stimulus_on_time, stimulus_off_time,
                                        responsetype_idx, responsetype_label)
    HabenulaCluster.Kmeans_ALL()


    # 1 : no reponse
